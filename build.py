import json

temtems = json.load(open('temtem.json'))

for temtem in temtems:
  typeItems = '<img alt="{type}" title="{type}" height="45" width="45" src="./temtem.wiki.gg/icons/{type}.webp">'.format(type=temtem['type'][0])

  for extraType in temtem['type'][1:]:
    typeItems += '<img alt="{type}" title="{type}" height="45" width="45" src="./temtem.wiki.gg/icons/{type}.webp">'.format(type=extraType)

  text = '''<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="UTF-8"/>
    <title>{name}</title>
    <link rel="stylesheet" href="./temtem.css">
    <meta name="description" content="{tempedia}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="./temtem.wiki.gg/icons/{name}.webp">
    <link rel="manifest" href="./manifest.json">
    <meta name="theme-color" content="#1a1a1a">
    <script type="module" src="./app.js"></script>
    <script type="module" src="./luma.js"></script>
  </head>
  <body>
    <h1>{name}</h1>
    <figure>
      <audio autoplay src="./temtem.wiki.gg/cry/{name}.opus"></audio>
      <video alt="TemTem" width="320" height="320" autoplay loop src="./temtem.wiki.gg/idle animations/{name}.webm"></video>
      <figcaption>#{no}</figcaption>
    </figure>
    <p>{tempedia}</p>
    <dl>
      <dt>Type</dt>
      <dd>
        {typeItems}
      </dd>
      <dt>Height</dt>
      <dd>{height} cm</dd>
      <dt>Weight</dt>
      <dd>{weight} kg</dd>
    </dl>
    <nav>
      <a href="{prev}.html" rel="prev"><img alt="{prev}" width="80" height="80" src="./temtem.wiki.gg/icons/{prev}.webp"></a>
      <a href="{next}.html" rel="next"><img alt="{next}" width="80" height="80" src="./temtem.wiki.gg/icons/{next}.webp"></a>
    </nav>
    <footer>
      <a rel="noreferrer" href="https://temtem.wiki.gg/{name}"><img src="./temtem.wiki.gg/icons/Site-logo.webp" height="128" width="233" alt="TemTem Wiki"></a>
    </footer>
  </body>
</html>'''.format(name=temtem['name'], tempedia=temtem['tempedia'], no=temtem['no'], typeItems=typeItems, height=temtem['height'], weight=temtem['weight'], prev=temtem['prev'], next=temtem['next'])

  file = open('{}.html'.format(temtem['name']),'w')
  file.write(text)
  file.close()
