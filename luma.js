// luma appear at a 1 in 7500 chance in the wild
// two luma parents have a 1 in 100 chance of producing a luma child
// to make it more likely visitors see lumas, lets use the breeding number


// Replace the temtem with the luma variant randomly
if (Math.random() < 1 / 100) {
  const temtem = document.querySelector('video[alt="TemTem"]');
  const [_, path, filename] = /(.+\/)(\w+.webm)/.exec(temtem.src);

  // Check to see luma's are available
  try {
    const response = await fetch(`${path}luma/${filename}`);
    temtem.src = `${path}luma/${filename}`
  }
  catch(e) {
    // it is alright if the fetch fails
    console.log(e)
  }
}
